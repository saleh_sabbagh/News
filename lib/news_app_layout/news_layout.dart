import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../news_app_screen/search/search_screen.dart';
import '/shared/components/components.dart';
import './/shared/cubit/cubit.dart';
import 'cubit/cubit.dart';
import 'cubit/states.dart';


class NewsLayout extends StatelessWidget {
  const NewsLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsCubit,NewsStates>(
        listener: (context,state) => {},
        builder: (context,state){

          var cubit = NewsCubit.get(context);
          return Scaffold(
            appBar: AppBar(
              title: const Text("NewsApp"),
              actions:  [
                IconButton(
                  icon : const Icon(Icons.search),
                  onPressed: (){
                    navigateTo(context, SearchScreen());
                  },
                ),
                IconButton(
                  icon : const Icon(Icons.brightness_4_outlined),
                  onPressed: (){
                    AppCubit.get(context).changeMode();
                  },
                ),
              ],
            ),
            body: cubit.screen[cubit.currentIndex],
            bottomNavigationBar: BottomNavigationBar(
              items: cubit.bottomItems ,
              currentIndex: cubit.currentIndex,
              onTap: (index){
                cubit.changeBottomNavIndex(index);
              },
            ),
          );
        }
    );
  }
}
