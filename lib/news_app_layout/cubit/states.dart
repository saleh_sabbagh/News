abstract class NewsStates {}

class NewsInitialState extends NewsStates {}

class NewsBottomSheetNavState extends NewsStates {}

// business

class NewsGetBusinessSuccessState extends NewsStates {}

class NewsLoadingBusinessSuccessState extends NewsStates {}

class NewsGetBusinessErrorState extends NewsStates {
  final error;

  NewsGetBusinessErrorState(this.error);
}

//sports

class NewsGetSportsSuccessState extends NewsStates {}

class NewsLoadingSportsSuccessState extends NewsStates {}

class NewsGetSportsErrorState extends NewsStates {
  final error;

  NewsGetSportsErrorState(this.error);
}

//science

class NewsGetScienceSuccessState extends NewsStates {}

class NewsLoadingScienceSuccessState extends NewsStates {}

class NewsGetScienceErrorState extends NewsStates {
  final error;

  NewsGetScienceErrorState(this.error);
}

//search

class NewsGetSearchSuccessState extends NewsStates {}

class NewsLoadingSearchSuccessState extends NewsStates {}

class NewsGetSearchErrorState extends NewsStates {
  final error;

  NewsGetSearchErrorState(this.error);
}

class NewsChangeModeState extends NewsStates {}

class NewsWebViewLoading extends NewsStates {}
