
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_app_test/news_app_layout/cubit/states.dart';

import '../../../shared/network/remote/dio_helper.dart';
import '../../news_app_screen/business/business_screen.dart';
import '../../news_app_screen/science/science_screen.dart';
import '../../news_app_screen/sports/sports_screen.dart';
class NewsCubit extends Cubit<NewsStates> {
  NewsCubit() : super(NewsInitialState());

  //Object
  static NewsCubit get(context) => BlocProvider.of(context);

  int currentIndex = 0;
  double progress = 0 ;
  bool? isDirectionRtl;

  void WebViewLoading(progress){
    this.progress = progress/100;
    emit(NewsWebViewLoading());
  }

  List<BottomNavigationBarItem> bottomItems = [
    const BottomNavigationBarItem(
        icon: Icon(Icons.business), label: 'Business'),
    const BottomNavigationBarItem(icon: Icon(Icons.sports), label: 'Sports'),
    const BottomNavigationBarItem(icon: Icon(Icons.science), label: 'Science'),
  ];

  List<Widget> screen = [
    const BusinessScreen(),
    const SportsScreen(),
    const ScienceScreen(),
  ];

  void changeBottomNavIndex(int index) {
    currentIndex = index;
    emit(NewsBottomSheetNavState());
  }

  //business
  List<dynamic> business = [];

  void getBusiness() {
    emit(NewsLoadingBusinessSuccessState());
    DioHelper.getData(
      url: 'v2/top-headlines',
      query: {
        'country': 'eg',
        'category': 'business',
        'apiKey': 'a51c2038802d4b9c82cfd6b952f17a05',
      },
    ).then((value) {
      business = value.data['articles'];
      // print(business[0]['url']);
      emit(NewsGetBusinessSuccessState());
    }).catchError((error) {
      print(error.toString());

      emit(NewsGetBusinessErrorState(error));
    });
  }

  //sports
  List<dynamic> sports = [];

  void getSports() {
    emit(NewsLoadingSportsSuccessState());
    if (sports.isEmpty) {
      emit(NewsGetSportsSuccessState());
      DioHelper.getData(
        url: 'v2/top-headlines',
        query: {
          'country': 'eg',
          'category': 'sports',
          'apiKey': 'a51c2038802d4b9c82cfd6b952f17a05',
        },
      ).then((value) {
        sports = value.data['articles'];
      }).catchError((error) {
        print(error.toString());
        emit(NewsGetSportsErrorState(error));
      });
    } else {
      emit(NewsGetSportsSuccessState());
    }
  }

  //science
  List<dynamic> science = [];

  void getScience() {
    emit(NewsLoadingScienceSuccessState());
    if (science.isEmpty) {
      DioHelper.getData(
        url: 'v2/top-headlines',
        query: {
          'country': 'eg',
          'category': 'science',
          'apiKey': 'a51c2038802d4b9c82cfd6b952f17a05',
        },
      ).then((value) {
        science = value.data['articles'];
        emit(NewsGetScienceSuccessState());
      }).catchError((error) {
        print(error.toString());
        emit(NewsGetScienceErrorState(error));
      });
    } else {
      emit(NewsGetScienceSuccessState());
    }
  }

  //search
  List<dynamic> search = [];

  void getSearch(String value) {
    emit(NewsLoadingSearchSuccessState());

    DioHelper.getData(
      url: 'v2/everything',
      query: {
        'q': value,
        'apiKey': 'a51c2038802d4b9c82cfd6b952f17a05',
      },
    ).then((value) {
      search = value.data['articles'];
      emit(NewsGetSearchSuccessState());
    }).catchError((error) {
      print(error.toString());
      emit(NewsGetSearchErrorState(error));
    });
  }
}
