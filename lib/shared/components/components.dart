import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../../news_app_screen/web_view/web_view_screen.dart';

Widget myDivider() {
  return Padding(
    padding: const EdgeInsetsDirectional.only(start: 22.0),
    child: Container(
      padding: const EdgeInsetsDirectional.only(start: 10.0),
      color: Colors.grey[300],
      height: 1,
    ),
  );
}

Widget defaultLoadingAnimation() {
  return Center(
      child: LoadingAnimationWidget.discreteCircle(
          size: 60.0, color: Colors.blue));
}

Widget buildArticleItem(article, context, bool direction) {
  return InkWell(
    onTap: () {
      navigateAndFinish(context, WebViewScreen(article['url']));
    },
    child: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        children: [
          Container(
            height: 120.0,
            width: 120.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              image: DecorationImage(
                image: NetworkImage('${article['urlToImage']}'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(width: 15.0),
          Expanded(
              child: SizedBox(
                  height: 120.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          '${article['title']}',
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.bodyText1,
                          textDirection: direction == true ? TextDirection.rtl : TextDirection.ltr,
                        ),
                      ),
                      Text(
                        '${intl.DateFormat('EEEE M-d  hh:mm a').format(DateTime.parse(article['publishedAt']))}',
                        style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 15.0,
                        ),
                      ),
                    ],
                  ))),
        ],
      ),
    ),
  );
}

Widget articleBuilder(context, list, bool direction) {
  return ListView.separated(
      itemBuilder: (context, index) => buildArticleItem(list[index], context, direction),
      separatorBuilder: (context, index) => myDivider(),
      physics: const BouncingScrollPhysics(),
      itemCount: list.length);
}

void navigateTo(context, widget) {
  Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ));
}

void navigateAndFinish(context, widget) {
  Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => widget), (route) => true);
}

