import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../news_app_layout/cubit/cubit.dart';
import '../../news_app_layout/cubit/states.dart';
import '/shared/components/components.dart';

class ScienceScreen extends StatelessWidget {
  const ScienceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsCubit, NewsStates>(
        listener: (context, state) {},
        builder: (context, state) {
          var cubit = NewsCubit.get(context);
          return cubit.science.isNotEmpty
              ? articleBuilder(context, cubit.science , cubit.isDirectionRtl = true)
              : defaultLoadingAnimation();
        });
  }
}
