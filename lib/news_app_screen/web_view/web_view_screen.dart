import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
class WebViewScreen extends StatefulWidget {

  late String url;

  WebViewScreen(this.url);

  @override
  State<WebViewScreen> createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  late bool showSpinner = true;

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: showSpinner,
      child: WebView(
        initialUrl:  widget.url ,
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: (finished){
          setState(() {
            showSpinner = false;
          });
        },
      ),
    );
  }
}
